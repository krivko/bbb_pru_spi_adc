#! /bin/bash

echo "-----Building pru_firmware-----"
        cd pru_firmware
	make clean
	make
	cd ..

echo "-----Building user application-----"
        cd user_app
        make clean
        make
	cd ..

echo "-----Placing the firmware to PRU-----"
        sudo cp ./pru_firmware/result/am335x-pru0-fw /lib/firmware/am335x-pru0-fw
        sudo cp ./pru_firmware/result/am335x-pru1-fw /lib/firmware/am335x-pru1-fw

echo "-----Configuring pinmux-----"
        config-pin -a P9.27 pruout
        config-pin -a P9.29 pruout
        config-pin -a P9.28 pruin
        config-pin -a P9.30 pruout

echo "-----Rebooting and start PRU-----"
        sudo sh -c "echo '4a338000.pru1' > /sys/bus/platform/drivers/pru-rproc/unbind"
        sudo sh -c "echo '4a338000.pru1' > /sys/bus/platform/drivers/pru-rproc/bind"
        sudo sh -c "echo '4a334000.pru0' > /sys/bus/platform/drivers/pru-rproc/unbind"
        sudo sh -c "echo '4a334000.pru0' > /sys/bus/platform/drivers/pru-rproc/bind"


echo "-----Start user application - press Enter-----"
read
        sudo ./user_app/fork_pcm_pru

echo "-----Stop PRU-----"
        sudo sh -c "echo '4a334000.pru0' > /sys/bus/platform/drivers/pru-rproc/unbind"
        sudo sh -c "echo '4a338000.pru1' > /sys/bus/platform/drivers/pru-rproc/unbind"

