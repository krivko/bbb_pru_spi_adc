#include <fcntl.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define CNT_SAMPLES_PER_SEC 100
#define SIZE_INPUT_BUF (CNT_SAMPLES_PER_SEC * sizeof(uint16_t))

int main(int argc, char **argv) {
  int pru_data, pru_clock; // file descriptors

  //  Now, open the PRU character device.
  //  Read data from it in chunks and write to the named pipe.
  ssize_t readpru, prime_char, pru_clock_command;

  pru_data = open("/dev/rpmsg_pru30", O_RDWR);
  if (pru_data < 0){
    printf("Failed to open pru character device rpmsg_pru30.\n");
  }

  //  The character device must be "primed".
  prime_char = write(pru_data, "prime", 6);
  if (prime_char < 0){
    printf("Failed to prime the PRU0 char device.\n");
  }

  //  Now open the PRU1 clock control char device and start the clock.
  pru_clock = open("/dev/rpmsg_pru31", O_RDWR);
  pru_clock_command = write(pru_clock, "g", 2);
  if (pru_clock_command < 0){
    printf("The pru clock start command failed.\n");
  }

  //  This is the main data transfer loop.
  //  Note that the number of transfers is finite.
  //  This can be changed to a while(1) to run forever.
  for (int i = 0; i < 10; i++) {
    uint8_t buf[SIZE_INPUT_BUF];
    readpru = read(pru_data, buf, SIZE_INPUT_BUF);
    uint8_t cnt_samples = readpru / sizeof(uint16_t);
    if (cnt_samples){
      uint32_t sum = 0;
      for (int j = 0; j < cnt_samples; j++){
        sum += *((uint16_t *)&buf[j * 2]);
      }
      printf("mean of %d samples = %d\n", cnt_samples, sum / cnt_samples);
    }
  }

  close(pru_data);
  close(pru_clock);
}
